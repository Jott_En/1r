#--------------------------------------------#
# Jirka Neumann
# 2023
# 1R classification
# using "Auto" Dataset of the "ISLR" Library
#--------------------------------------------#

library(ISLR)

quantilize <- function(this_col){
  col_num = which(colnames(auto_data) == this_col)
  my_qu = quantile(auto_data[, col_num])

  auto_data[, col_num][auto_data[, col_num] < my_qu[2]] <- 1 #25%
  auto_data[, col_num][auto_data[, col_num] >= my_qu[2] & auto_data[, col_num] < my_qu[3]] <- 2 #50%
  auto_data[, col_num][auto_data[, col_num] >= my_qu[3] & auto_data[, col_num] < my_qu[4]] <- 3 #75%
  auto_data[, col_num][auto_data[, col_num] >= my_qu[4]] <- 4 #100%

  return(auto_data[, col_num])
}


do_loops = 15 #--> define how many times the classification is to be repeated
train_extent = 0.7 #--> define percentage of train data
#--> 2do: balance_meth = 1 #--> choose 1 for downsampling and 2 for upsampling


auto_data = NULL
auto_data_train = NULL
auto_data_test = NULL
success_rate_mean = seq(0,0, length.out = do_loops)

itz = 1
while (itz <= do_loops) {


#--------------------#
#--- 1. read data ---#
#--------------------#

require(ISLR)
data("Auto")
#auto_data = data("Auto") #--> doesn't work
auto_data = Auto #--> how to avoid this step and load data directly into auto_data..?


#-----------------------#
#--- 2. prepare data ---#
#-----------------------#

auto_data = auto_data[auto_data$origin != 2,] #-- remove lowest quantity (2 = Europe) to make it binomial

#--> prepare result_df which will contain the error rate of each column
md_names = head(names(auto_data), -2) #--> remove last two Elements (origin and names), not being classified
result_df = as.data.frame(cbind(Attribute = md_names, Gesamtfehler = NA, Fehlerquote = NA), stringsAsFactors = FALSE)
prognos_list = list() #--> will be filled within the loop


#--> replace values by its quantiles
auto_data$mpg = quantilize("mpg")

##############--> keep $cylinders Values
#--> in case two Variables have the same error rate T-Test can be applied
#auto_data$Cylinders[auto_data$Cylinders <= 6] <- 1
#auto_data$Cylinders[auto_data$Cylinders > 6] <- 2
##############--> Cylinders

auto_data$displacement = quantilize("displacement")
auto_data$horsepower = quantilize("horsepower")
auto_data$weight = quantilize("weight")
auto_data$acceleration = quantilize("acceleration")
#--> keep $year Values


#-----------------------#
#--- 3. balance data ---#
#-----------------------#

l_01 = length(auto_data$origin[auto_data$origin == 1]) #--> US
l_02 = length(auto_data$origin[auto_data$origin == 3]) #--> non-US (Japan)

#----- downsampling -----#
if(l_01 != l_02){
  if(l_01 > l_02){
    l_cut = sample(1:l_01, l_01 - l_02, replace = FALSE) #--> sample values, FALSE = no duplicate values
    auto_data_01 = auto_data[auto_data$origin == 1,]
    auto_data_01 = auto_data_01[-l_cut,] #--> subtract sample values
    auto_data_02 = auto_data[auto_data$origin == 3,]
  }else if(l_01 < l_02){
    l_cut = sample(1:l_02, l_02 - l_01, replace = FALSE)
    auto_data_01 = auto_data[auto_data$origin == 1,]
    auto_data_02 = auto_data[auto_data$origin == 3,]
    auto_data_02 = auto_data_02[-l_cut,]
  }
}


#----------------------------------------#
#--- 4. divide data in train and test ---#
#----------------------------------------#

l_01 = length(auto_data_01$origin[auto_data_01$origin == 1])
l_02 = length(auto_data_02$origin[auto_data_02$origin == 3])

#--------------------------------------------------
if(l_01 == l_02){
  n_train_cut = round(l_01 * train_extent) #--> extent of train data, the rest to test data
  n_train_cut = sample(1:l_01, n_train_cut, replace = FALSE) #--> sample values, FALSE = no duplicate values
  
  auto_data_01_train = auto_data_01[n_train_cut,] #--> neues df mit x% Datensätzen
  auto_data_01_test = auto_data_01[-n_train_cut,] #--> neues df ohne x% Datensätzen
  
  auto_data_02_train = auto_data_02[n_train_cut,] #--> neues df mit x% Datensätzen
  auto_data_02_test = auto_data_02[-n_train_cut,] #--> neues df ohne x% Datensätzen
  
  #--> combine dataframes
  auto_data_train = rbind(auto_data_01_train, auto_data_02_train) #--> auto_data neu beschreiben, nur mit EU+US
  auto_data_test = rbind(auto_data_01_test, auto_data_02_test) #--> auto_data neu beschreiben, nur mit EU+US
  
  #--> clean up environment
  rm(auto_data_01, auto_data_01_train, auto_data_01_test, auto_data_02, auto_data_02_train, auto_data_02_test)
  rm(l_01, l_02, l_cut, n_train_cut)
  
}else{print("An error occurred: Data not balanced."); break}
#--------------------------------------------------    



#-------------------------------#
#--- 5. train classification ---#
#-------------------------------#

auto_data = auto_data_train


#--- prepare loop ---#
md_l = length(md_names) #--> count cols to be evaluated, md_names defined in 2.
F_Quote = seq(0,0, length.out = md_l) #--> prepare vector for error rate (length --> md_l)
i_Anzahl = seq(0,0, length.out = md_l) #--> prepare vector for counts (length --> md_l)

test_vec = NULL #--> check counts
prognos_vec = NULL #--> vector for the respective prediction values

#----------------- big while-loop ---#
i = 1
while (i <= md_l){
  
  md_ufac = unique(auto_data[,i]) #--> count unique factors in column i

  print(paste0("Attribut_", i, ": ", md_names[i], " hat ", length(md_ufac), " Faktoren: "))
  print(" ")
  
  prognos_vec = as.character(result_df[i, 1]) #--> prognos_vec wird nach der Schleife in die prognos_list übertragen

  #------------------- aditional for-loop --#
  for (iz in md_ufac){
    print(iz)
    print(" ")
    
    Sp_Fac = auto_data[auto_data[, i] == iz,] #-- i.te Spalte nach iz.tem Wert filtern

    #--- Häufigkeiten ermitteln ---#    
    Anzahl = length(Sp_Fac$origin) #--> count observations in iz-charactaristic of depending Variable (origin)

    #--> je nach 1 und nach 3 filtern
    Sp_Fac_1 = Sp_Fac[Sp_Fac$origin == 1,] #--> i.te Spalte nach iz.tem Wert filtern

    Sieg_x = length(Sp_Fac_1$origin) #--> Anzahl Siege für iz.ten Wert
    Lose_x = Anzahl - Sieg_x #--> Anzahl Niederlagen für iz.ten Wert
    
    print(paste0("Siege: ", Sieg_x, " von ", Anzahl))
    print(paste0("Lose: ", Lose_x, " von ", Anzahl))
    
    
    if (Sieg_x >= Lose_x) {
      prognos_vec = append(prognos_vec, c(as.character(iz), "Sieg")) #--> wird nach der Schleife in die prognos_list übertragen
      F_Quote_einzel = Lose_x #--> temporäre Fehlerquote ist gleich dem jeweils anderen Wert
      print(paste0(iz, ": Sieg prognostiziert."))
    } else {
      prognos_vec = append(prognos_vec, c(as.character(iz), "Lose")) #--> wird nach der Schleife in die prognos_list übertragen
      F_Quote_einzel = Sieg_x #--> temporäre Fehlerquote ist gleich dem jeweils anderen Wert
      print(paste0(iz, ": Lose prognostiziert."))
    }
    
    
    test_vec = append(test_vec, Anzahl) #--> Testvector zum Festhalten von Anzahl
    
    print(paste0("Fehlerquote: ", F_Quote_einzel, " von ", Anzahl))
    print(" ")
    
    
    F_Quote[i] = F_Quote[i] + F_Quote_einzel #--> Gesamtfehlerquote ausrechnen/zusammenaddieren
    i_Anzahl[i] = i_Anzahl[i] + Anzahl #--> Gesamtanzahl ausrechnen/zusammenaddieren
    
  }
  #----------------- for-loop end ---#
  
  
  result_df[i,2] = paste0(F_Quote[i], " von ", i_Anzahl[i]) #--> zweite Spalte im result_df beschreiben
  result_df[i,3] = F_Quote[i] / i_Anzahl[i] #--> Fehlerquote ausrechnen und in die zweite Spalte im result_df schreiben
  
  prognos_list = append(prognos_list, list(prognos_vec)) #--> prediction vector being passed on to the list
  prognos_vec = NULL #--> prediction vector being reset
  
  print(paste0("Gesamtfehler: ", F_Quote[i]))
  print(paste0("Gesamtanzahl: ", i_Anzahl[i]))
  print(paste0("Gesamtfehlerquote: ", F_Quote[i], " von ", i_Anzahl[i]))
  print(paste0("Entspricht: ", F_Quote[i] / i_Anzahl[i]))
  print(" ")
  print("+-+-+_________________________________+-+-+")
  print(" ")
  
  if (i == 15){break} #--> safety break
  i = i + 1;
}
#----------------- big while-loop end ---#


#--> clean environment
rm(Anzahl, F_Quote, F_Quote_einzel, i_Anzahl, Lose_x, md_l, md_names, md_ufac, Sieg_x, test_vec)


min_FQ = min(result_df$Fehlerquote)
min_FQ = subset(result_df, Fehlerquote == min_FQ)
min_FQ = min_FQ[1, 1]
print(" ")
print(paste0("Das Attribut mit der geringsten Fehlerquote lautet: ", min_FQ))
print(" ")
print(" ")

#--> prediction values being put together
i = 1
while (i <= length(prognos_list)){
  
  if (prognos_list[[i]][1] == min_FQ){ #--> da wo der Listeneintrag gleich dem Minimum-Wert ist
    
    z = 2 #--> z beginnt bei 2 weil der erste Eintrag bereits im vorherigen Schritt abgeglichen wurde
    while (z <= length(prognos_list[[i]])){
      
      prognos_vec_i = i #--> save position of prediction values
      
      if (prognos_list[[i]][z + 1] == "Lose"){progausgabe = "non-US"}
      if (prognos_list[[i]][z + 1] == "Sieg"){progausgabe = "US"}
      print(paste0("Wenn ", print(min_FQ), " den Wert ", prognos_list[[i]][z], " annimmt, wird ", progausgabe, " vorhergesagt."))
      

      print(" ")
      z = z + 2
    }  
  }
  
  i = i + 1
  if (i >= 15){break} #--> safety break
}



#-----------------------------------------------------#
#--- 6. apply classification training to test data ---#
#-----------------------------------------------------#



prognos_vec = prognos_list[[prognos_vec_i]] #--> prognos_vec wird neu beschrieben mit dem gewünschten Listeneintrag

m_pos = match(min_FQ, names(auto_data_test)) #--> min_FQ enthält das Sieger-Attribut, in welcher Spalte befindet es sich?



auto_data_test$prediction = NA #--> new column for prediction values


i = 2

while(i <= length(prognos_vec)){
  
  Wert1 = as.numeric(prognos_vec[i]) #--> turn character to numeric
  Wert2 = (prognos_vec[i + 1])
  if (Wert2 == "Lose"){Wert2 = 3}
  if (Wert2 == "Sieg"){Wert2 = 1}
  
  auto_data_test$prediction[auto_data_test[, m_pos] == Wert1] <- Wert2 #--> filter column m_pos for Wert1 and pass on to $prediction Wert2
  
  i = i + 2
  if (i >= 25){break} #--> safety break
}



#--------------------------------------------#
#--- 7. calculate success- and error-rate ---#
#--------------------------------------------#


Filter_T1 = subset(auto_data_test, origin == 1)
Treffer1 = nrow(subset(Filter_T1, prediction == 1))
Fehler1 = nrow(Filter_T1) - Treffer1

Filter_T2 = subset(auto_data_test, origin == 3)
Treffer2 = nrow(subset(Filter_T2, prediction == 3))
Fehler2 = nrow(Filter_T2) - Treffer2


TrefferQuote = (Treffer1 + Treffer2) / (Treffer1 + Treffer2 + Fehler1 + Fehler2)
FehlerQuote = (Fehler1 + Fehler2) / (Treffer1 + Treffer2 + Fehler1 + Fehler2)

print(paste0("Treffer-Quote: ", TrefferQuote, " %"))
print(" ")
print(paste0("Fehler-Quote: ", FehlerQuote, " %"))


success_rate_mean[itz] = TrefferQuote

itz = itz + 1
if (itz > do_loops){break} #--> safety break
}
#--> Gesamtschleife Ende <-----

#--> clean environment
rm(Filter_T1, Filter_T2, Sp_Fac, Fehler1, Fehler2, FehlerQuote, progausgabe, Treffer1, Treffer2, TrefferQuote, Wert1, Wert2)


#--------------------------------#
#--- 7.1 average success rate ---#
#--------------------------------#

print("")
print(paste0(itz - 1, " runs of the script have resulted in an average success rate of ", round(mean(success_rate_mean), 4), " %."))
print("")

summary(success_rate_mean)


#-----------------------------------------------------------------------------#
#-------------------------------- Script Ende --------------------------------#
#-----------------------------------------------------------------------------#




