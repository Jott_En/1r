1R is a simple classifier for binary characteristics to be classified. For more information look up online...

2023, Jirka Neumann
-------------------

The 1R classifier in the following script is written in R language.
Using the "Auto" dataset of the "ISLR" package it has an average success rate slightly above 80%.
The script tries to abstain from R libraries/packages and is fun to play with...
-------------------

The script is 'work in progress' in many parts. Upcoming features may include:

* an alternative input possibility for data
* improved variable names and all comments in english
* provide upsampling as an alternative to downsampling (any exciting effects on the success rate??) 
* add a new column to check and compare origin an prediction for easy reading
* recheck the break-comands at the end of the script (except the do_loop break) - are they really necessary?
* translate all print-outputs to english and possibly provide an improved report
* apply T-Test for variables that have the same error rate


